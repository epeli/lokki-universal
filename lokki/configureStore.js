import { createStore, applyMiddleware, compose } from "redux";
import appReducers from "./reducers";
import thunk from "redux-thunk";
import logger from "redux-logger";
import { AsyncStorage } from "react-native";
import { loadStateFromStorage } from "./actions";

export default function configureStore(initialStore) {
  const middlewares = [thunk, logger]; // TODO: do not use "logger" in production
  const enhancer = compose(applyMiddleware(...middlewares));  
  let store = createStore(appReducers, initialStore, enhancer);

  // Loads stored data to Redux
  store.dispatch(loadStateFromStorage()); 

  return store;
}
