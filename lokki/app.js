import React from "react";
import { Text, AppState } from "react-native";
import { Provider } from "react-redux";
import configureStore from "./configureStore";
import { AppNavigator } from "./navigation/appNavigator";
import { saveReduxStateToStorage } from "./actions";

const store = configureStore({});

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    AppState.addEventListener("change", this._handleAppStateChange.bind(this));
  }

  componentWillUnmount() {
    AppState.removeEventListener(
      "change",
      this._handleAppStateChange.bind(this)
    );
  }

  render() {
    return (
      <Provider store={store}>
        <AppNavigator />
      </Provider>
    );
  }

  _handleAppStateChange(currentAppState) {
    if (currentAppState === "background") {
      console.log("app to background");
      //store.dispatch(saveReduxStateToStorage());
      // NOTE: We store whole redux state to storage after every booking changes
      // on book.js
    } else if (currentAppState === "active") {
      console.log("app to active");
      // NOTE: this is not called on app init
      // configureStore.js loads default state from Storage
    }
  }

};

export default App;
