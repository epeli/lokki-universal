import {
  SAVE_BOOKINGS,
  REMOVE_BOOKINGS,
  MAX_DATES_TO_BOOK
} from "../constants";
import { getDateKeys, getNewDateFromDate } from "../helpers";

const initialState = {
  reservationsByDay: {}
};

export function getDateKey(date) {
  return date.getFullYear() + "" + (date.getMonth() + 1) + "" + date.getDate();
}

/*
 reservationsByDay: { 
   "dateKey": { 
     bookings: { 
       '1': { 
         name: 'Aaaa', phone: '', other: '' 
        } 
      } 
    } 
  }  
*/
export default function bookingReducer(state = initialState, action) {
  // http://www.benmvp.com/learning-es6-enhanced-object-literals/

  switch (action.type) {
    case SAVE_BOOKINGS: {
      const dateKey = getDateKey(action.date);
      const actionItemKey = action.item.key;

      // Insert / Update reservations by date
      const reservationsInState = state.reservationsByDay[dateKey];
      const reservations = reservationsInState
        ? { [dateKey]: { ...reservationsInState } }
        : { [dateKey]: { bookings: {} } };

      // Insert / Update booking by date
      const currDateBookings = reservations[dateKey].bookings;
      let currItemBookings = currDateBookings[actionItemKey];
      if (currItemBookings) {
        console.log("update another booking for the day");
        currItemBookings = {
          [actionItemKey]: { ...currItemBookings, ...action.booking }
        };
      } else {
        console.log("insert first booking for the day");
        currItemBookings = { [actionItemKey]: { ...action.booking } };
      }

      // Set bookings to reservations
      reservations[dateKey].bookings[actionItemKey] =
        currItemBookings[actionItemKey];
      state.reservationsByDay = { ...state.reservationsByDay, ...reservations };

      return {
        ...state
      };
    }
    case REMOVE_BOOKINGS: {
      const startDate = getNewDateFromDate(action.date, -MAX_DATES_TO_BOOK)
      let dateKeys = getDateKeys(startDate, MAX_DATES_TO_BOOK + MAX_DATES_TO_BOOK + 2); // 2 for sure
      let counter = 0;

      while (dateKeys.length > 0) {
        const dateKey = dateKeys.pop();
        let reservationsInState = state.reservationsByDay[dateKey];
        const ret = removeBookingItem(
          reservationsInState,
          action.item,
          action.userId
        );
        if (ret) {
          state.reservationsByDay[dateKey] = ret;
        }
        counter++;
      }

      return {
        ...state
      };
    }

    default:
      return state;
  }
}

function removeBookingItem(reservationsInDay, item, userId) {
  if (
    reservationsInDay &&
    reservationsInDay.bookings &&
    reservationsInDay.bookings[item.key] &&
    reservationsInDay.bookings[item.key].userId === userId
  ) {
    delete reservationsInDay.bookings[item.key];
    return reservationsInDay;
  } else {
    return null;
  }
}
