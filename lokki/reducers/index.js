import { combineReducers } from "redux";
import cottages from "./cottages";
import navRecuder from '../navigation/navReducer';
import booking from './booking';
import date from './date';

import {
  LOAD_STORAGE_TO_REDUX_STATE,
  SAVE_REDUX_STATE_TO_STORAGE
} from "../constants";

const reducers = combineReducers({
  nav: navRecuder,
  cottages,
  booking,
  date
});

// We want that LOAD_STORAGE_TO_REDUX_STATE updates whole reducer state
const tunedReducers = (state, action) => {
  if (action.type === LOAD_STORAGE_TO_REDUX_STATE) {
    console.log("Save AsyncStorage data to Redux");
    return {...action.data};
  }
  return reducers(state, action);
} 

export default tunedReducers;
