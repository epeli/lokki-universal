import {
  DEFAULT_COTTAGES,
  SAVE_COTTAGES,
  SAVE_BOOKINGS,
  REMOVE_BOOKINGS,
  SET_DATE,
  LOAD_STORAGE_TO_REDUX_STATE,
  SAVE_REDUX_STATE_TO_STORAGE
} from "./constants";
import { AsyncStorage } from "react-native";

export function saveCottages(data) {
  return {
    type: SAVE_COTTAGES,
    data
  };
}

export function saveBookings(date, item, booking) {
  return {
    type: SAVE_BOOKINGS,
    date,
    item,
    booking
  };
}

export function removeBookings(date, item, userId) {
  return {
    type: REMOVE_BOOKINGS,
    date,
    item,
    userId
  };
}

export function setDate(date) {
  return {
    type: SET_DATE,
    date
  };
}

export function loadStateFromStorage() {
  return dispatch => {
    AsyncStorage.getItem("completeStore")
      .then(value => {
        if (false && value && value.length) {
          try {
            const storageData = JSON.parse(value);
            dispatch(saveStorageToReduxState(storageData));
          } catch (error) {
            console.log("loadStateFromStorage JSON parser error", error);
          }
        } else {
          dispatch(saveCottages(DEFAULT_COTTAGES));
        }
      })
      .catch(error => {
        console.log("loadStateFromStorage error", error);
      });
  };
}

function saveStorageToReduxState(data) {
  return {
    type: LOAD_STORAGE_TO_REDUX_STATE,
    data
  };
}

export function saveReduxStateToStorage() {
  return (dispatch, getState) => {
    let reduxState = JSON.stringify(getState());
    AsyncStorage.setItem("completeStore", reduxState);
  };
}
