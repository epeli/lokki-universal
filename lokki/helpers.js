import { getDateKey } from "./reducers/booking";
import { MAX_DATES_TO_BOOK } from "./constants";

export const getNextFreeDates = (booking, date, item) => {
  const tomorrow = getNextDayFromDate(date);
  const nextDateKeys = getDateKeys(tomorrow, MAX_DATES_TO_BOOK);
  let ret = [];

  for (let index = 0; index < nextDateKeys.length; index++) {
    let dateKey = nextDateKeys[index];
    const reservationsByDay = booking.reservationsByDay[dateKey];
    if (reservationsByDay && reservationsByDay.bookings[item.key]) {
      break; // reserved
    } else {
      ret.push(new Date(date.getTime() + 24 * 60 * 60 * 1000 * index));
    }
  }
  return ret;
};

export const isSameDate = (date1, date2) => {
  return date1.getDate() === date2.getDate() &&
  date1.getMonth() === date2.getMonth() &&
  date1.getFullYear() === date2.getFullYear()
    ? true
    : false;
};

export const getNewDateFromDate = (date, transformationDates) => {
  return new Date(date.getTime() + 24 * 60 * 60 * (1000 * transformationDates));
};

export const getNextDayFromDate = date => {
  return new Date(date.getTime() + 24 * 60 * 60 * 1000);
};

export const getDateKeys = (date, amount) => {
  let ret = [];
  ret.push(getDateKey(date)); // first day
  for (var index = 1; index < amount + 1; index++) {
    ret.push(
      getDateKey(new Date(date.getTime() + 24 * 60 * 60 * 1000 * index))
    ); // next day
  }
  return ret;
};

export const getBooking = (booking, date, item) => {
  const dateKey = getDateKey(date);
  const reservationsByDay = booking.reservationsByDay[dateKey] || {
    bookings: {}
  };
  if (reservationsByDay && reservationsByDay.bookings && item) {
    return reservationsByDay.bookings[item.key];
  } else {
    return null;
  }
};

export const getFilteredCottages = (cottagesFromState, booking, date) => {
  const cottages = [...cottagesFromState];
  const dateKey = getDateKey(date);
  const reservationsByDay = booking.reservationsByDay[dateKey] || {
    bookings: {}
  };
  cottages.sort((c1, c2) => {
    const r1 = reservationsByDay.bookings[c1.key];
    const r2 = reservationsByDay.bookings[c2.key];
    if (r1 && !r2) {
      return 1;
    } else if (!r1 && r2) {
      return -1;
    } else {
      return 0;
    }
  });
  return cottages;
};

export const getUserId = () => {
  return Date.now();
};
