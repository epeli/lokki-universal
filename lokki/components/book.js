import React from "react";
import {
  View,
  StyleSheet,
  Image,
  TextInput,
  Text,
  ScrollView,
  Dimensions
} from "react-native";
import BackgroundView from "./backgroundView";
import EndDateView from "./endDateView";
import IconView from "./iconView";
import { connect } from "react-redux";
import { BOOKING_ITEM } from "../constants";
import { saveBookings, removeBookings, saveReduxStateToStorage } from "../actions";
import { getDateKey } from "../reducers/booking";
import {
  getNextFreeDates,
  getDateKeys,
  getBooking,
  getNextDayFromDate,
  isSameDate,
  getUserId
} from "../helpers";
import { TINT_COLOR } from "../constants";

var dateFormat = require("dateformat");

class BookView extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: "Book " + navigation.state.params.item.name,
    headerTintColor: "black"
  });

  constructor(props) {
    super(props);

    const { width, height } = Dimensions.get("window");
    const endDate = getNextDayFromDate(this.props.navigation.state.params.date);
    const name = this.props.booking.name || "";
    const userId = this.props.booking.userId || getUserId();

    this.state = {
      width,
      height,
      dirty: false,
      name,
      userId, 
      phone: this.props.booking.phone || "",
      other: this.props.booking.other || "",
      endDate,
      isEditing: name.length > 0 ? true : false
    };
    this.onDateSelected = this.onDateSelected.bind(this);
    this._remove = this._remove.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ ...nextProps.booking });
  }

  componentWillUnmount() {
    this._saveStateToRedux();
  }

  _getDateString(date) {
    // https://www.npmjs.com/package/dateformat
    return (
      "(W" + dateFormat(date, "W") + ") " + dateFormat(date, "ddd dd.mm.yyyy")
    );
  }

  render() {
    const { item, date } = this.props.navigation.state.params;
    const scrollViewStyle = [styles.scrollView, this._getHeight()];
    const containerStyle = [styles.container, this._getMargings()];

    return (
      <BackgroundView source={{ uri: item.img }}>
        <ScrollView style={scrollViewStyle} keyboardDismissMode={"on-drag"}>
          <View style={containerStyle}>
            <Text style={styles.date}>{this._getDateString(date)}</Text>
            <TextInput
              style={styles.input}
              value={this.state.name}
              onChangeText={text => this.setState({ name: text, dirty: true })}
              autoCorrect={false}
              placeholderTextColor={"red"}
              placeholder={"Customer"}
            />
            <TextInput
              style={styles.input}
              value={this.state.phone}
              onChangeText={text => this.setState({ phone: text, dirty: true })}
              autoCorrect={false}
              keyboardType={"numeric"}
              placeholderTextColor={"gray"}
              placeholder={"Phone"}
            />
            <TextInput
              style={[styles.input, styles.inputOther]}
              value={this.state.other}
              onChangeText={text => this.setState({ other: text, dirty: true })}
              autoCorrect={false}
              multiline={true}
              numberOfLines={2}
              placeholderTextColor={"gray"}
              placeholder={"Other"}
            />
            {this.props.freeDates && (
              <EndDateView
                dates={this.props.freeDates}
                style={styles.endDate}
                onDateSelected={this.onDateSelected}
              />
            )}
            {this.state.isEditing && (
              <IconView
                style={styles.icon}
                icon={"trash"}
                onPress={this._remove}
              />
            )}
          </View>
        </ScrollView>
      </BackgroundView>
    );
  }

  onDateSelected(date) {
    this.setState({ endDate: date, dirty: true });
  }

  _getHeight() {
    return {
      height: this.state.height,
      width: this.state.width
    };
  }

  _getMargings() {
    return {
      marginTop: 30,
      marginBottom: 10,
      marginLeft: 10,
      marginRight: 10
    };
  }

  _remove() {
    const { userId } = this.state;
    this.props.removeBooking(userId);
    this.props.navigation.goBack();
  }

  _saveStateToRedux() {
    const { userId, name, phone, other, dirty, endDate } = this.state;
    if (dirty && name.length > 0) {
      this.props.saveState(userId, name, phone, other, endDate);
    }
  }
}

/* -----------------------------------------------------------------
  mapStateToProps
*/
function mapStateToProps(state, ownProps) {
  const { date, item } = ownProps.navigation.state.params;
  return {
    booking: getBooking(state.booking, date, item) || BOOKING_ITEM,
    freeDates: getNextFreeDates(state.booking, date, item),
    item
  };
}

/* -----------------------------------------------------------------
  mapDispatchToProps
*/
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    saveState: (userId, name, phone, other, endDate) => {
      let { date, item } = ownProps.navigation.state.params;
      save(dispatch, { date, endDate, item, userId, name, phone, other });
      //dispatch(saveReduxStateToStorage());
    },
    removeBooking: (userId) => {
      let { date, item } = ownProps.navigation.state.params;
      dispatch(removeBookings(date, item, userId));     
      //dispatch(saveReduxStateToStorage());
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BookView);

/* -----------------------------------------------------------------
  Helper
*/
function save(dispatch, data) {
  const { date, endDate, item, userId, name, phone, other } = data;

  if (date && endDate && item) {
    let indexDate = new Date(date);
    while (!isSameDate(indexDate, endDate)) {
      console.log("Will save booking to date", indexDate);
      dispatch(saveBookings(indexDate, item, { userId, name, phone, other }));
      indexDate = getNextDayFromDate(indexDate);
    }
  }
}

/* -----------------------------------------------------------------
  StyleSheet
*/
const styles = StyleSheet.create({
  scrollView: {
    height: 400
  },
  container: {
    flex: 1,
    backgroundColor: "rgba(224, 224, 224, 0.8)",
    padding: 20,
    borderWidth: 1.0,
    borderColor: "white",
    borderRadius: 4
  },
  date: {
    backgroundColor: TINT_COLOR,
    lineHeight: 40,
    textAlign: "center",
    color: "white",
    fontSize: 25
  },
  endDate: {
    marginTop: 20
  },
  input: {
    marginTop: 20,
    height: 35,
    fontSize: 25,
    backgroundColor: "white"
  },
  inputOther: {
    height: 80,
    fontSize: 20
  },
  icon: {
    marginTop: 20,
    fontSize: 40,
    width: 50,
    height: 50,
    alignSelf: "flex-end",
    color: "red",
    textAlign: "center"
  }  
});
