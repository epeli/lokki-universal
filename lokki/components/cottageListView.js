import React from "react";
import { Text, View, FlatList, StyleSheet } from "react-native";
import { connect } from "react-redux";
import CottageListItem from "./cottageListItem";
import { getDateKey } from "../reducers/booking";
import { dateToString, dateStringToDate } from "../reducers/date";
import { getFilteredCottages } from "../helpers";

class CottageListView extends React.Component {
  constructor(props) {
    super(props);

    this._cottageSelected = this._cottageSelected.bind(this);
    this._renderItem = this._renderItem.bind(this);
    this._getData = this._getData.bind(this);
  }

  render() {
    return (
      <View style={this.props.style}>
        <FlatList
          style={styles.list}
          data={this._getData()}
          renderItem={this._renderItem}
        />
      </View>
    );
  }

  _getData() {
    return this.props.cottages;
  }

  _renderItem({ item }) {
    return (
      <CottageListItem
        id={item.key}
        item={item}
        onPress={this._cottageSelected}
      />
    );
  }

  _cottageSelected(data) {
    const { date } = this.props;
    this.props.navigation.navigate("Book", {
      item: data.item,
      date
    });
  }
}

const styles = StyleSheet.create({
  list: {
    flex: 1
  }
});

/* -----------------------------------------------------------------
  mapStateToProps
*/
function mapStateToProps(state, ownProps) {
  const date = dateStringToDate(state.date.activeDate);
  return {
    cottages: getFilteredCottages(
      state.cottages.list,
      state.booking,
      date
    ),
    date
  };
}

/* -----------------------------------------------------------------
  mapDispatchToProps
*/
function mapDispatchToProps(dispatch) {
  return {};
}



export default connect(mapStateToProps, mapDispatchToProps)(CottageListView);
