import React from "react";
import { Image } from "react-native";

export default class BackgroundView extends React.Component {

  /*
  setNativeProps = (nativeProps) => {
    this._root.setNativeProps(nativeProps);
  }*/

  render() {
    return (
      <Image
        ref={component => this._root = component}
        source={this.props.source}
        style={[{
          flex: 1,
          width: null,
          height: null,
          resizeMode: "cover"
        },this.props.style]}
      >
      {this.props.children}
      </Image>
    );
  }
}
