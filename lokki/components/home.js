import React from "react";
import { View, StyleSheet, Image } from "react-native";
import { connect } from "react-redux";
import { DEFAULT_COTTAGES } from "../constants";
import CottageListView from "./cottageListView";
import BackgroundView from "./backgroundView";
import DateView from "./dateView";
import { CalendarList } from "react-native-calendars";
import { setDate } from "../actions";
import { dateToString, dateStringToDate } from "../reducers/date";
import { TINT_COLOR } from "../constants";

// https://github.com/wix/react-native-calendars

var dateFormat = require("dateformat");

class Home extends React.Component {
  static navigationOptions = {
    title: "Cottages",
    headerTintColor: "black"
  };

  constructor(props) {
    super(props);
    this.state = { showCalendar: false };
    this._toggleCalendar = this._toggleCalendar.bind(this);
    this._hideCalendar = this._hideCalendar.bind(this);
    this._onDayPress = this._onDayPress.bind(this);
  }

  render() {
    return (
      <View style={styles.container}>
        <BackgroundView source={require("../gfx/etusivu.png")}>
          <DateView
            style={styles.date}
            fontSize={20}
            toggleCalendar={this._toggleCalendar}
            hideCalendar={this._hideCalendar}
          />
          <CottageListView
            style={styles.list}
            navigation={this.props.navigation}
          />
          {this.state.showCalendar && (
            <CalendarList
              style={styles.calendar}
              current={this.props.activeDate}
              firstDay={1}
              theme={{
                selectedDayBackgroundColor: TINT_COLOR
              }}
              markedDates={this._getMarkedDates()}
              onDayPress={this._onDayPress}
            />
          )}
        </BackgroundView>
      </View>
    );
  }

  _getMarkedDates() {
    const s = dateFormat(this.props.activeDate, "yyyy-mm-dd");
    return {
      [s]: { marked: false, selected: true }
    };
  }

  _onDayPress(day) {
    /*
  { year: 2017,
    month: 9,
    day: 6,
    timestamp: 1504656000000,
    dateString: '2017-09-06' 
  } */
    this._hideCalendar();
    this.props.setActiveDate(new Date(day.dateString));
  }

  _toggleCalendar() {
    this.setState({ showCalendar: !this.state.showCalendar });
  }

  _hideCalendar() {
    this.setState({ showCalendar: false });
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  date: {
    height: 50,
    backgroundColor: "black"
  },
  list: {
    flex: 1,
    backgroundColor: "rgba(0,0,0,0)"
  },
  calendar: {
    position: "absolute",
    top: 60,
    bottom: 10,
    left: 20,
    right: 20
  }
});

/* -----------------------------------------------------------------
  mapStateToProps
*/
function mapStateToProps(state, ownProps) {
  return {
    activeDate: dateStringToDate(state.date.activeDate)
  };
}

/* -----------------------------------------------------------------
  mapDispatchToProps
*/
function mapDispatchToProps(dispatch, ownProps) {
  return {
    setActiveDate(date) {
      dispatch(setDate(date));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
