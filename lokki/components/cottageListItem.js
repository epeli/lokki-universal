import React from "react";
import {
  Text,
  View,
  FlatList,
  StyleSheet,
  TouchableHighlight
} from "react-native";
import { connect } from "react-redux";
import BackgroundView from "./backgroundView";
import { dateToString, dateStringToDate } from "../reducers/date";

import { getDateKey } from "../reducers/booking";
import { getBooking } from "../helpers";

class CottageListItem extends React.Component {
  constructor(props) {
    super(props);
    this._onPress = this._onPress.bind(this);
  }

  render() {
    return (
      <BackgroundView source={{ uri: this.props.item.img }}>
        <TouchableHighlight
          onPress={this._onPress}
          underlayColor={"rgba(206, 228, 217, 0.4)"}
        >
          <View style={styles.container}>
            <Text style={styles.text}>{this.props.item.name}</Text>
            {this.props.booking && (
              <Text style={styles.booking}>
                {this.props.booking.name}
              </Text>
            )}
          </View>
        </TouchableHighlight>
      </BackgroundView>
    );
  }

  _onPress() {
    this.props.onPress({ item: this.props.item, id: this.props.id });
  }
}

const styles = StyleSheet.create({
  container: {
    height: 140,
    justifyContent: "flex-start",
    alignItems: "center",
    flexDirection: "row",
    borderBottomWidth: 1.0,
    borderBottomColor: "white"
  },
  text: {
    left: 20,
    fontSize: 100,
    color: "white",
    textShadowColor: "black",
    textShadowOffset: { width: 1, height: 1 }
  },
  booking: {
    color: "white",
    fontSize: 30,
    left: 40,
    right: 10,
    textShadowColor: "black",
    textShadowOffset: { width: 1, height: 1 }
  }
});

/* -----------------------------------------------------------------
  mapStateToProps
*/
function mapStateToProps(state, ownProps) {
  const date = dateStringToDate(state.date.activeDate);
  return {
    date,
    booking: getBooking(
      state.booking,
      date,
      ownProps.item
    )
  };
}

/* -----------------------------------------------------------------
  mapDispatchToProps
*/
function mapDispatchToProps(dispatch) {
  return {};
}


export default connect(mapStateToProps, mapDispatchToProps)(CottageListItem);
