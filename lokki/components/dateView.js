import React from "react";
import { View, Text, StyleSheet, TouchableHighlight } from "react-native";
import { connect } from "react-redux";
import IconView from "./iconView";
import { setDate } from "../actions";
import { dateToString, dateStringToDate } from "../reducers/date";

var dateFormat = require("dateformat");

class DateView extends React.Component {
  constructor(props) {
    super(props);
    this._nextDay = this._nextDay.bind(this);
    this._prevDay = this._prevDay.bind(this);
  }

  render() {
    const dateStyle = { fontSize: this.props.fontSize || 20 };
    return (
      <View style={[styles.container, this.props.style]}>
        <IconView
          style={styles.icon}
          icon={"chevron-circle-left"}
          onPress={this._prevDay}
        />
        <TouchableHighlight onPress={this.props.toggleCalendar}>
          <Text style={[styles.date, dateStyle]}>{this._getDateString()}</Text>
        </TouchableHighlight>
        <IconView
          style={styles.icon}
          icon={"chevron-circle-right"}
          onPress={this._nextDay}
        />
      </View>
    );
  }

  _nextDay() {
    this.props.hideCalendar();
    this.props.moveToNextDay(this.props.date);
  }

  _prevDay() {
    this.props.hideCalendar();
    this.props.moveToPreviousDay(this.props.date);
  }

  // https://www.npmjs.com/package/dateformat
  _getDateString() {
    return (
      "(W" +
      dateFormat(this.props.date, "W") +
      ") " +
      dateFormat(this.props.date, "ddd dd.mm.yyyy")
    );
  }
}

const styles = StyleSheet.create({
  container: {
    //flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  },
  date: {
    color: "white",
    textAlign: "center"
  },
  icon: {
    fontSize: 30,
    padding: 15,
    color: "white"
  }
});

/* -----------------------------------------------------------------
  mapStateToProps
*/
function mapStateToProps(state, ownProps) {
  return {
    date: dateStringToDate(state.date.activeDate)
  };
}

/* -----------------------------------------------------------------
  mapDispatchToProps
*/
function mapDispatchToProps(dispatch, ownProps) {
  return {
    moveToNextDay(currDate) {
      var tomorrow = new Date(currDate.getTime() + 24 * 60 * 60 * 1000);
      dispatch(setDate(tomorrow));
    },
    moveToPreviousDay(currDate) {
      var yesterday = new Date(currDate.getTime() - 24 * 60 * 60 * 1000);
      dispatch(setDate(yesterday));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DateView);
