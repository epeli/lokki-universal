React Native application for Android and iOS. 

With this application user keeps track on his cottage bookings.

Keywords: React Native, Redux, AsyncStorage, Calendar, Fonts Awesome

![home](https://bytebucket.org/teropaananen/lokki-universal/raw/06504045afb1f3fd55efd1edbccc105759791bd1/example.png)


![calendar](https://bytebucket.org/teropaananen/lokki-universal/raw/5b90af71b592368a6ec738187d31e3d685b33d4d/calendar.png)


![booking](https://bytebucket.org/teropaananen/lokki-universal/raw/ea7f26bd39e379e67bb06e7109301269d795fe67/booking.png)

